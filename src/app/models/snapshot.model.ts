export class Snapshot {
  id: number;
  name: string;
  description: string;
}
