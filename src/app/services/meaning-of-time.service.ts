import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DayModel} from '../models/day.model';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {MeaningOfTimeModel} from '../models/meaning-of-time.model';

@Injectable({
  providedIn: 'root'
})
export class MeaningOfTimeService {

  constructor(private http: HttpClient) { }

  getMeaningOfTimes(): Observable <MeaningOfTimeModel[]> {
    return this.http.get<MeaningOfTimeModel[]>(environment.api_url + 'meaning_of_times.json').pipe(share());
  }
}
