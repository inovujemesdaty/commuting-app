import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {CommuteDirectionModel} from '../models/commute-direction.model';

@Injectable({
  providedIn: 'root'
})
export class CommuteDirectionService {

  constructor(private http: HttpClient) { }

  getCommuteDirections(): Observable <CommuteDirectionModel[]> {
    return this.http.get<CommuteDirectionModel[]>(environment.api_url + 'commute_directions.json').pipe(share());
  }
}
