import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';
import {Snapshot} from '../models/snapshot.model';

@Injectable({
  providedIn: 'root'
})
export class SnapshotService {

  constructor(private http: HttpClient) { }

  getSnapshots(): Observable <Snapshot[]> {
    return this.http.get<Snapshot[]>(environment.api_url + 'snapshots.json');
  }
}
