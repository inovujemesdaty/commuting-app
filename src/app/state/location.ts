import {State, Action, StateContext, NgxsOnInit} from '@ngxs/store';
import {LocationService} from '../services/location.service';
import {tap} from 'rxjs/operators';
import { Location} from '../models/location.model';

export class GetLocations {
  static readonly type = '[Location] Get Locations';
}

export interface LocationStateModel {
  locations: Location[];
}

@State<LocationStateModel>({
  name: 'location',
  defaults: {
    locations: []
  }
})
export class LocationState implements NgxsOnInit {
  constructor(private locationService: LocationService) {}

  ngxsOnInit(ctx: StateContext<LocationStateModel>) {
    ctx.dispatch(new GetLocations());
  }
​
  @Action(GetLocations)
  getLocations(ctx: StateContext<LocationStateModel>, action: GetLocations) {
    return this.locationService.getLocations().pipe(tap((locations) => {
      const state = ctx.getState();
      ctx.setState({
        ...state,
        locations: locations
      });
    }));
  }
}
