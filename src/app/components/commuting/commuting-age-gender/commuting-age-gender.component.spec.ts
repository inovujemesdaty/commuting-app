import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingAgeGenderComponent } from './commuting-age-gender.component';

describe('CommutingAgeGenderComponent', () => {
  let component: CommutingAgeGenderComponent;
  let fixture: ComponentFixture<CommutingAgeGenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingAgeGenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingAgeGenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
