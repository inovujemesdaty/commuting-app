import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DayModel} from '../models/day.model';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {DistanceModel} from '../models/distance.model';

@Injectable({
  providedIn: 'root'
})
export class DistanceService {

  constructor(private http: HttpClient) { }

  getDistances(): Observable <DistanceModel[]> {
    return this.http.get<DistanceModel[]>(environment.api_url + 'distances.json').pipe(share());
  }
}
