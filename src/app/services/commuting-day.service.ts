import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlgenService} from './urlgen.service';
import {Observable} from 'rxjs';
import {CommutingDayModel} from '../models/commuting-day.model';
import {share} from 'rxjs/operators';
import {AppSettings} from '../app.settings';

@Injectable({
  providedIn: 'root'
})
export class CommutingDayService {

  constructor(private http: HttpClient, private urlGen: UrlgenService ) { }

  getCommutingDay(snapshots, locations): Observable <CommutingDayModel[]> {
    this.urlGen.setExtension('json');
    this.urlGen.setUrlAction('commuting_days');
    this.urlGen.addParam('snapshot', snapshots);
    this.urlGen.addParam('location', locations);
    if (this.urlGen.isParameterSet('snapshot') && this.urlGen.isParameterSet('location')) {
      return this
        .http
        .get<CommutingDayModel[]>(this.urlGen.getUrl())
        .pipe(share());
    } else {
      return null;
    }
  }

  getTopCommutingDayGraphjsData(input) {
    if (input.length === 0) {
      return;
    }
    input = input.sort(function (a, b) {
      return b.count - a.count;
    });
    let graphData = {
      datasets: [{
        data: [],
        backgroundColor: [],
        label: 'TOP migrace dle pravidelnosti'
      }],
      labels: []
    };
    input.forEach((data, index) => {
      if (index < AppSettings.topMigrationCount) {
        graphData.labels.push(data.location.name + ' - ' + data.commuteDirection.name +
          ' ' + data.day.name
          + ' - ' + data.count + ' (' + data.snapshot.name + ')'
        );
        graphData.datasets[0].data.push(data.count);
        graphData.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[index]);
      }
    });
    return graphData;
  }

  getCommutingDayGraphjsData(input) {
    if (input.length === 0) {
      return;
    }

    return;
  }
}
