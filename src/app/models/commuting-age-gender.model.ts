import {Snapshot} from './snapshot.model';
import {Location} from './location.model';
import {GenderModel} from './gender.model';
import {AgeModel} from './age.model';
import {CommuteDirectionModel} from './commute-direction.model';

export class CommutingAgeGenderModel {
  id: number;
  snapshot: Snapshot;
  location: Location;
  gender: GenderModel;
  ageGroup: AgeModel;
  commuteDirection: CommuteDirectionModel;
  count: number;
}
