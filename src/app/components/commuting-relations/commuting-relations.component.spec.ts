import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingRelationsComponent } from './commuting-relations.component';

describe('CommutingRelationsComponent', () => {
  let component: CommutingRelationsComponent;
  let fixture: ComponentFixture<CommutingRelationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingRelationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingRelationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
