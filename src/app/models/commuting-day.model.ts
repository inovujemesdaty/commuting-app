import {Snapshot} from './snapshot.model';
import {Location} from './location.model';
import {GenderModel} from './gender.model';
import {AgeModel} from './age.model';
import {CommuteDirectionModel} from './commute-direction.model';
import {DayModel} from './day.model';

export class CommutingDayModel {
  id: number;
  snapshot: Snapshot;
  location: Location;
  day: DayModel;
  commuteDirection: CommuteDirectionModel;
  count: number;
}
