import { TestBed, inject } from '@angular/core/testing';

import { UrlgenService } from './urlgen.service';

describe('UrlgenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlgenService]
    });
  });

  it('should be created', inject([UrlgenService], (service: UrlgenService) => {
    expect(service).toBeTruthy();
  }));
});
