import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UrlgenService} from './urlgen.service';
import {Observable} from 'rxjs';
import {CommutingRelationsModel} from '../models/commuting-relations.model';
import {share} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import {AppSettings} from '../app.settings';
import * as L from 'leaflet';


@Injectable({
  providedIn: 'root'
})
export class CommutingRelationsService {

  constructor(private http: HttpClient, private urlGen: UrlgenService ) { }

  getCommutingBetweenLocations(snapshots, locations, destinations): Observable <CommutingRelationsModel[]> {
    this.urlGen.setExtension('json');
    this.urlGen.setUrlAction('commuting_between_locations');
    this.urlGen.addParam('snapshot', snapshots);
    this.urlGen.addParam('sourceLocation', locations);
    this.urlGen.addParam('destinationLocation', destinations);
    if (this.urlGen.isParameterSet('snapshot') && (
      this.urlGen.isParameterSet('sourceLocation') || this.urlGen.isParameterSet('destinationLocation')
    )) {
      return this
        .http
        .get<CommutingRelationsModel[]>(this.urlGen.getUrl())
        .pipe(share());
    } else {
      return null;
    }
  }

  getTopCommutingBetweenGraphjsData(input) {
    if (input.length === 0) {
      return;
    };
    input = input.sort(function (a, b) {
      return b.count - a.count;
    });
    let graphData = {
      datasets: [{
        data: [],
        backgroundColor: [],
        label: 'TOP migrace'
      }],
      labels: []
    };
    input.forEach((data, index) => {
      if (index < AppSettings.topMigrationCount) {
        graphData.labels.push(data.sourceLocation.name + ' -> ' + data.destinationLocation.name
          + ' - ' + data.count + ' (' + data.snapshot.name + ')'
        );
        graphData.datasets[0].data.push(data.count);
        graphData.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[index]);
      }
    });
    return graphData;
  }

  getCommutingBetweenGraphjsData(input) {
    if (input.length === 0) {
      return;
    };
    let graphData = [];
    input.forEach((data) => {
      if (graphData[data.snapshot.id] === undefined) {
        graphData[data.snapshot.id] = {
          id: data.snapshot.id,
          name: data.snapshot.name,
          data: {
            datasets: [{
              data: [],
              backgroundColor: [],
              label: 'Migrace ' + data.snapshot.name
            }],
            labels: []
          }
        };
      }
    });
    input.forEach((data) => {
      var length = graphData[data.snapshot.id].data.datasets[0].data.length;
      if (length < 10 && ((length < 2 ) ||
        data.count / graphData[data.snapshot.id].data.datasets[0].data[1]  > 0.02)){
        graphData[data.snapshot.id].data.labels.push(
          data.sourceLocation.name + ' -> ' + data.destinationLocation.name + ' - ' + data.count
        );
        graphData[data.snapshot.id].data.datasets[0].data.push(data.count);
        graphData[data.snapshot.id].data.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[length]);
      }
    });
    let graphDataFinal = [];
    graphData.forEach((data) => {
      if(data !== undefined) {
        graphDataFinal.push(data);
      }
    });
    graphDataFinal.sort(function(a, b) {
      return a.id - b.id;
    })
    return graphDataFinal;
  }

  getReturnedLayers(locations, selected, layers, fromLocations) {
    let selectedArray = [];
    let selectedLocations = layers;
    selected.forEach(s => {
      selectedArray.push(s.destinationLocation.id);
    });
    locations.forEach(l => {
      if (fromLocations.indexOf(l.id) !== -1) {
        selectedLocations.push(
          L.geoJSON(JSON.parse(l.border),
            { style: () => ({ color: '#D62F2F' })
            }));
      }
      if (selectedArray.indexOf(l.id) !== -1) {
        selectedLocations.push(
          L.geoJSON(JSON.parse(l.border),
            { style: () => ({ color: '#367DC4' })
            }));
      }
    });
    return selectedLocations;
  }

}
