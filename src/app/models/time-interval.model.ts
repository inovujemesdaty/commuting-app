export class TimeIntervalModel {
  id: number;
  name: string;
  description: string;
}
