import { TestBed, inject } from '@angular/core/testing';

import { CommutingDayService } from './commuting-day.service';

describe('CommutingDayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingDayService]
    });
  });

  it('should be created', inject([CommutingDayService], (service: CommutingDayService) => {
    expect(service).toBeTruthy();
  }));
});
