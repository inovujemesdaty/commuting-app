import {Snapshot} from './snapshot.model';
import {Location} from './location.model';
import {GenderModel} from './gender.model';
import {AgeModel} from './age.model';
import {CommuteDirectionModel} from './commute-direction.model';
import {DayModel} from './day.model';
import {DistanceModel} from './distance.model';
import {DurationModel} from './duration.model';
import {RegularityModel} from './regularity.model';

export class CommutingRegularityModel {
  id: number;
  snapshot: Snapshot;
  location: Location;
  regularity: RegularityModel;
  commuteDirection: CommuteDirectionModel;
  count: number;
}
