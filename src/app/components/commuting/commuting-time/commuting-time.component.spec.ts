import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingTimeComponent } from './commuting-time.component';

describe('CommutingTimeComponent', () => {
  let component: CommutingTimeComponent;
  let fixture: ComponentFixture<CommutingTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
