import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Snapshot} from '../../../models/snapshot.model';
import {Location} from '../../../models/location.model';
import {AppSettings} from '../../../app.settings';
import {UrlgenService} from '../../../services/urlgen.service';
import {CommutingTimeModel} from '../../../models/commuting-time.model';
import {CommutingTimeService} from '../../../services/commuting-time.service';
import {CommuteDirectionModel} from '../../../models/commute-direction.model';
import {MeaningOfTimeModel} from '../../../models/meaning-of-time.model';
import {TimeIntervalModel} from '../../../models/time-interval.model';

@Component({
  selector: 'app-commuting-time',
  templateUrl: './commuting-time.component.html'
})
export class CommutingTimeComponent implements OnInit, OnChanges {

  @Input() public selectedSnapshots$: Observable<Snapshot[]>;
  @Input() public selectedLocations$: Observable<Location[]>;
  @Input() public commuteDirections$: Observable<CommuteDirectionModel[]>;
  @Input() public meaningsOfTime: Observable<MeaningOfTimeModel[]>;
  @Input() public timeIntervals: Observable<TimeIntervalModel[]>;

  public topGraphData;
  public messages = AppSettings.ngxTableMessages;

  public topGraphType = 'doughnut';
  public topGraphOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    title: {
      display: false,
      text: ''
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  public rows$: Observable<CommutingTimeModel[]>;
  public columns = [
    { prop: 'location.name', 'name' : 'Zkoumaná oblast' },
    { prop: 'location.type', 'name' : 'Typ' },
    { prop: 'commuteDirection.name', 'name' : 'Typ migrace' },
    { prop: 'meaningOfTime.name', 'name' : 'Význam' },
    { prop: 'timeInterval.name', 'name' : 'Časový rozsah' },
    { prop: 'snapshot.name', 'name' : 'Snapshot' },
    { prop: 'count', 'name' : 'Počet pracujících' },
  ];
  public indicator = false;
  public locations = [];

  public csvUrl = '';

  colorScheme = {
    domain: AppSettings.graphColors.domain
  };

  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  constructor(
    private commutingService: CommutingTimeService,
    private urlGen: UrlgenService) {}

  ngOnInit() {
    // this.updateData();
  }
  ngOnChanges() {
    this.updateData();
  }

  updateData() {
    this.indicator = true;
    this.rows$ = this.commutingService.getCommutingTime(
      this.selectedSnapshots$,
      this.selectedLocations$
    );
    if (this.rows$ !== null) {
      this.rows$.subscribe((x) => {
          this.indicator = false;
          this.topGraphData = this.commutingService.getTopCommutingTimeGraphjsData(x);
        },
        () => {
          this.indicator = false;
        },
        () => {
          this.indicator = false;
        });
      this.csvUrl = this.urlGen.setUrlAction('commuting_times')
        .setExtension('csv')
        .addParam('snapshot', this.selectedSnapshots$)
        .addParam('sourceLocation', this.selectedLocations$)
        .getUrl();
    } else {
      this.indicator = false;
      this.csvUrl = '';
      this.topGraphData = {};
    }
  }

}
