import { TestBed, inject } from '@angular/core/testing';

import { RegularityService } from './regularity.service';

describe('RegularityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegularityService]
    });
  });

  it('should be created', inject([RegularityService], (service: RegularityService) => {
    expect(service).toBeTruthy();
  }));
});
