import {Snapshot} from './snapshot.model';
import {Location} from './location.model';
import {GenderModel} from './gender.model';
import {AgeModel} from './age.model';
import {CommuteDirectionModel} from './commute-direction.model';
import {DayModel} from './day.model';
import {DistanceModel} from './distance.model';

export class CommutingDistanceModel {
  id: number;
  snapshot: Snapshot;
  location: Location;
  distance: DistanceModel;
  commuteDirection: CommuteDirectionModel;
  count: number;
}
