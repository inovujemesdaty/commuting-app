import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlgenService} from './urlgen.service';
import {Observable} from 'rxjs';
import {CommutingDurationModel} from '../models/commuting-duration.model';
import {share} from 'rxjs/operators';
import {AppSettings} from '../app.settings';

@Injectable({
  providedIn: 'root'
})
export class CommutingDurationService {

  constructor(private http: HttpClient, private urlGen: UrlgenService ) { }

  getCommutingDuration(snapshots, locations): Observable <CommutingDurationModel[]> {
    this.urlGen.setExtension('json');
    this.urlGen.setUrlAction('commuting_durations');
    this.urlGen.addParam('snapshot', snapshots);
    this.urlGen.addParam('location', locations);
    if (this.urlGen.isParameterSet('snapshot') && this.urlGen.isParameterSet('location')) {
      return this
        .http
        .get<CommutingDurationModel[]>(this.urlGen.getUrl())
        .pipe(share());
    } else {
      return null;
    }
  }

  getTopCommutingDurationGraphjsData(input) {
    if (input.length === 0) {
      return;
    };
    input = input.sort(function (a, b) {
      return b.count - a.count;
    });
    let graphData = {
      datasets: [{
        data: [],
        backgroundColor: [],
        label: 'TOP migrace dle délky trvání'
      }],
      labels: []
    };
    input.forEach((data, index) => {
      if (index < AppSettings.topMigrationCount) {
        graphData.labels.push(data.location.name + ' - ' + data.commuteDirection.name +
          ' ' + data.duration.name
          + ' - ' + data.count + ' (' + data.snapshot.name + ')'
        );
        graphData.datasets[0].data.push(data.count);
        graphData.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[index]);
      }
    });
    return graphData;
  }
}
