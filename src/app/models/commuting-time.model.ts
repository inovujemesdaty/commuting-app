import {Snapshot} from './snapshot.model';
import {Location} from './location.model';
import {CommuteDirectionModel} from './commute-direction.model';
import {MeaningOfTimeModel} from './meaning-of-time.model';
import {TimeIntervalModel} from './time-interval.model';

export class CommutingTimeModel {
  id: number;
  snapshot: Snapshot;
  location: Location;
  meaningOfTime: MeaningOfTimeModel;
  timeInterval: TimeIntervalModel;
  commuteDirection: CommuteDirectionModel;
  count: number;
}
