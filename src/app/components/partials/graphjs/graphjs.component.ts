import {Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

import Chart from 'chart.js';

@Component({
  selector: 'app-graphjs',
  template: '',
  styleUrls: ['./graphjs.component.css']
})
export class GraphjsComponent implements OnInit, OnChanges {

  chart: any;

  @Input() type: string;
  @Input() data: any;
  @Input() options: any;
  @Input() typeSelections: any;
  @Output() clickCanvas = new EventEmitter();
  @Output() clickDataset = new EventEmitter();
  @Output() clickElements = new EventEmitter();
  @Output() clickElement = new EventEmitter();

  private canvas;

  private controls;

  constructor(private elementRef: ElementRef, private ngZone: NgZone) { }

  ngOnInit() {
    this.create();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.chart) {
      if (changes['type'] || changes['options']) {
        this.create();
      } else if (changes['data']) {
        let currentValue = changes['data'].currentValue;
        ['datasets', 'labels', 'xLabels', 'yLabels'].forEach(property => {
          this.chart.data[property] = currentValue[property];
        })
        this.chart.update();
      }
    } else {
      this.create();
    }
  }

  private create() {
    this.ngZone.runOutsideAngular(() => {
      if (this.canvas) {
        this.elementRef.nativeElement.removeChild(this.canvas);
      }
      if (this.controls) {
        this.elementRef.nativeElement.removeChild(this.controls);
      }
      if (this.typeSelections !== undefined) {
        this.controls = document.createElement('div');
        this.controls.className = 'text-right';
        this.typeSelections.forEach((data, index) => {
         let button = document.createElement('button');
         button.innerText = data.name;
         button.className = 'btn btn-light btn-sm';
         button.onclick = (e) => {
           this.type = data.type;
           this.create();
         };
         this.controls.appendChild(button);
        })
        this.elementRef.nativeElement.appendChild(this.controls);
      }
      this.canvas = document.createElement('canvas');
      this.elementRef.nativeElement.appendChild(this.canvas);
      this.chart = new Chart(this.canvas, {
        type: this.type,
        data: this.data,
        options: this.options
      });
      this.canvas.onclick = e => {
        this.ngZone.run(() => {
          this.clickCanvas.next(e);
          if (this.clickDataset.observers.length) {
            this.clickDataset.next(this.chart.getDatasetAtEvent(e));
          }
          if (this.clickElements.observers.length) {
            this.clickElements.next(this.chart.getElementsAtEvent(e));
          }
          if (this.clickElement.observers.length) {
            this.clickElement.next(this.chart.getElementAtEvent(e));
          }
        });
      };
    });
  }
}
