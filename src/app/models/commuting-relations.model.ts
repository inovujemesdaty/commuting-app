import {Snapshot} from './snapshot.model';
import {Location} from './location.model';

export class CommutingRelationsModel {
  id: number;
  snapshot: Snapshot;
  sourceLocation: Location;
  destinationLocation: Location;
  count: number;
}
