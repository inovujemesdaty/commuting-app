import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Snapshot} from '../../../models/snapshot.model';
import {Location} from '../../../models/location.model';
import {AppSettings} from '../../../app.settings';
import {UrlgenService} from '../../../services/urlgen.service';
import {CommutingAgeGenderModel} from '../../../models/commuting-age-gender.model';
import {CommutingAgeGenderService} from '../../../services/commuting-age-gender.service';
import {CommuteDirectionModel} from '../../../models/commute-direction.model';
import {GenderModel} from '../../../models/gender.model';
import {AgeModel} from '../../../models/age.model';

@Component({
  selector: 'app-commuting-age-gender',
  templateUrl: './commuting-age-gender.component.html'
})
export class CommutingAgeGenderComponent implements OnInit, OnChanges {

  @Input() public selectedSnapshots$: Observable<Snapshot[]>;
  @Input() public selectedLocations$: Observable<Location[]>;
  @Input() public genders$: Observable<GenderModel[]>;
  @Input() public ages$: Observable<AgeModel[]>;
  @Input() public commuteDirections$: Observable<CommuteDirectionModel[]>;

  public topGraphData;
  public messages = AppSettings.ngxTableMessages;

  public topGraphType = 'doughnut';
  public topGraphOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    title: {
      display: false,
      text: ''
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  public rows$: Observable<CommutingAgeGenderModel[]>;
  public columns = [
    { prop: 'location.name', 'name' : 'Zkoumaná oblast' },
    { prop: 'location.type', 'name' : 'Typ' },
    { prop: 'gender.name', 'name' : 'Pohlaví' },
    { prop: 'ageGroup.name', 'name' : 'Věk' },
    { prop: 'commuteDirection.name', 'name' : 'Typ migrace' },
    { prop: 'snapshot.name', 'name' : 'Snapshot' },
    { prop: 'count', 'name' : 'Počet pracujících' },
  ];
  public indicator = false;
  public locations = [];

  public csvUrl = '';

  colorScheme = {
    domain: AppSettings.graphColors.domain
  };

  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  constructor(
    private commutingService: CommutingAgeGenderService,
    private urlGen: UrlgenService) {}

  ngOnInit() {
    // this.updateData();
  }
  ngOnChanges() {
    this.updateData();
  }

  updateData() {
    this.indicator = true;
    this.rows$ = this.commutingService.getCommutingAge(
      this.selectedSnapshots$,
      this.selectedLocations$
    );
    if (this.rows$ !== null) {
      this.rows$.subscribe((x) => {
          this.indicator = false;
          this.topGraphData = this.commutingService.getTopCommutingAgeGenderGraphjsData(x);
        },
        () => {
          this.indicator = false;
        },
        () => {
          this.indicator = false;
        });
      this.csvUrl = this.urlGen.setUrlAction('commuting_age_genders')
        .setExtension('csv')
        .addParam('snapshot', this.selectedSnapshots$)
        .addParam('sourceLocation', this.selectedLocations$)
        .getUrl();
    } else {
      this.indicator = false;
      this.csvUrl = '';
      this.topGraphData = {};
    }
  }

}
