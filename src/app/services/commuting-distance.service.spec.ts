import { TestBed, inject } from '@angular/core/testing';

import { CommutingDistanceService } from './commuting-distance.service';

describe('CommutingDistanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingDistanceService]
    });
  });

  it('should be created', inject([CommutingDistanceService], (service: CommutingDistanceService) => {
    expect(service).toBeTruthy();
  }));
});
