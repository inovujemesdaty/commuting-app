import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {Snapshot} from '../../../models/snapshot.model';
import {Location} from '../../../models/location.model';
import {AppSettings} from '../../../app.settings';
import {UrlgenService} from '../../../services/urlgen.service';
import {CommutingDayModel} from '../../../models/commuting-day.model';
import {CommutingDayService} from '../../../services/commuting-day.service';
import {DayModel} from '../../../models/day.model';
import {CommuteDirectionModel} from '../../../models/commute-direction.model';

@Component({
  selector: 'app-commuting-day',
  templateUrl: './commuting-day.component.html'
})
export class CommutingDayComponent implements OnInit, OnChanges {

  @Input() public selectedSnapshots$: Observable<Snapshot[]>;
  @Input() public selectedLocations$: Observable<Location[]>;
  @Input() public days$: Observable<DayModel[]>;
  @Input() public commuteDirections$: Observable<CommuteDirectionModel[]>;

  public topGraphData;
  public graphData;
  public messages = AppSettings.ngxTableMessages;

  public topGraphType = 'doughnut';
  public topGraphOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    title: {
      display: false,
      text: ''
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  public rows$: Observable<CommutingDayModel[]>;
  public columns = [
    { prop: 'location.name', 'name' : 'Zkoumaná oblast' },
    { prop: 'location.type', 'name' : 'Typ' },
    { prop: 'day.name', 'name' : 'Den' },
    { prop: 'commuteDirection.name', 'name' : 'Typ migrace' },
    { prop: 'snapshot.name', 'name' : 'Snapshot' },
    { prop: 'count', 'name' : 'Počet pracujících' },
  ];
  public indicator = false;
  public locations = [];

  public csvUrl = '';

  colorScheme = {
    domain: AppSettings.graphColors.domain
  };

  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  constructor(
    private commutingService: CommutingDayService,
    private urlGen: UrlgenService) {}

  ngOnInit() {
    // this.updateData();
  }
  ngOnChanges() {
    this.updateData();
  }

  updateData() {
    this.indicator = true;
    this.rows$ = this.commutingService.getCommutingDay(
      this.selectedSnapshots$,
      this.selectedLocations$
    );
    if (this.rows$ !== null) {
      this.rows$.subscribe((x) => {
          this.indicator = false;
          this.topGraphData = this.commutingService.getTopCommutingDayGraphjsData(x);
          this.graphData = this.commutingService.getCommutingDayGraphjsData(x);
        },
        () => {
          this.indicator = false;
        },
        () => {
          this.indicator = false;
        });
      this.csvUrl = this.urlGen.setUrlAction('commuting_days')
        .setExtension('csv')
        .addParam('snapshot', this.selectedSnapshots$)
        .addParam('sourceLocation', this.selectedLocations$)
        .getUrl();
    } else {
      this.indicator = false;
      this.csvUrl = '';
      this.topGraphData = {};
    }
  }
}
