import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingRegularityComponent } from './commuting-regularity.component';

describe('CommutingRegularityComponent', () => {
  let component: CommutingRegularityComponent;
  let fixture: ComponentFixture<CommutingRegularityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingRegularityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingRegularityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
