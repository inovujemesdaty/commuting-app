import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {DayModel} from '../models/day.model';

@Injectable({
  providedIn: 'root'
})
export class DayService {

  constructor(private http: HttpClient) { }

  getDays(): Observable <DayModel[]> {
    return this.http.get<DayModel[]>(environment.api_url + 'days.json').pipe(share());
  }
}
