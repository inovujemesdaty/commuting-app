import {AfterViewChecked, AfterViewInit, Component, Input, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {Observable} from 'rxjs';
import {Snapshot} from '../../models/snapshot.model';
import {SnapshotService} from '../../services/snapshot.service';
import {Location} from '../../models/location.model';
import {LocationService} from '../../services/location.service';
import {ActivatedRoute} from '@angular/router';
import {Select, Store} from '@ngxs/store';
import {DayService} from '../../services/day.service';
import {DistanceService} from '../../services/distance.service';
import {DurationService} from '../../services/duration.service';
import {GenderService} from '../../services/gender.service';
import {RegularityService} from '../../services/regularity.service';
import {TimeIntervalService} from '../../services/time-interval.service';
import {MeaningOfTimeService} from '../../services/meaning-of-time.service';
import {DayModel} from '../../models/day.model';
import {TimeIntervalModel} from '../../models/time-interval.model';
import {RegularityModel} from '../../models/regularity.model';
import {MeaningOfTimeModel} from '../../models/meaning-of-time.model';
import {GenderModel} from '../../models/gender.model';
import {DurationModel} from '../../models/duration.model';
import {DistanceModel} from '../../models/distance.model';
import {CommuteDirectionModel} from '../../models/commute-direction.model';
import {CommuteDirectionService} from '../../services/commute-direction.service';

@Component({
  selector: 'app-commuting',
  templateUrl: './commuting.component.html'
})
export class CommutingComponent implements OnInit, AfterViewInit {

  public snapshots$: Observable<Snapshot[]>;
  @Select(state => state.location.locations) locations$: Observable<Location[]>;
  public locations = [];
  public selectedSnapshots$ = [];
  // public selectedSnapshots$: Observable<Snapshot[]>;
  public selectedLocations$: Observable<Location[]>;
  public layers = [];
  public activeTab;
  public map: L.Map;
  // options
  showLegend = true;

  public options = {
    layers: [
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 15, minZoom: 6})
    ],
    zoom: 7,
    center: L.latLng( 49.774169, 15.604379)
  };

  goptions = {
    responsive: true,
    maintainAspectRatio: false
  };

  public days$: Observable<DayModel[]>;
  public distances$: Observable<DistanceModel[]>;
  public durations$: Observable<DurationModel[]>;
  public genders$: Observable<GenderModel[]>;
  public meaningOfTime$: Observable<MeaningOfTimeModel[]>;
  public regularities$: Observable<RegularityModel[]>;
  public timeIntervals$: Observable<TimeIntervalModel[]>;
  public commuteDirections$: Observable<CommuteDirectionModel[]>;

  constructor(
    private snapshotService: SnapshotService,
    private locationService: LocationService,
    private commuteDirectionService: CommuteDirectionService,
    private dayService: DayService,
    private distanceService: DistanceService,
    private durationService: DurationService,
    private genderService: GenderService,
    private meaningOfTimeService: MeaningOfTimeService,
    private regularityService: RegularityService,
    private timeIntervalService: TimeIntervalService,
    private activeRoute: ActivatedRoute,
    private store: Store) {
    this.activeTab = 'age-gender';
  }

  ngOnInit() {
    this.snapshots$ = this.snapshotService.getSnapshots();
    // this.locations$ = this.locationService.getLocations();
    this.locations$.subscribe(data => {
      this.locations = data;
    });
  }

  onLocationChange($event) {
    this.map.invalidateSize();
    this.layers = $event.map(city => {
      return L.geoJSON(JSON.parse(city.border),
        { style: () => ({ color: '#D62F2F', fillOpacity: 0.45 })});
    });
  }

  onMapReady(map: L.Map) {
    this.map = map;
  }

  ngAfterViewInit() {
    this.map.invalidateSize();
    this.map.panTo([49.774169, 15.604379]);
  }

  setActiveTab(key: string) {
    this.activeTab = key;
    setTimeout(() => {
      // Hack for table columns recalculation
      window.dispatchEvent(new Event('resize'));
    }, (50));
  }

  scrollToEl(el: HTMLElement) {
    el.scrollIntoView({behavior: "smooth", block: "start"});
  }
}
