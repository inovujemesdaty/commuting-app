import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {TimeIntervalModel} from '../models/time-interval.model';

@Injectable({
  providedIn: 'root'
})
export class TimeIntervalService {

  constructor(private http: HttpClient) { }

  getTimeIntervals(): Observable <TimeIntervalModel[]> {
    return this.http.get<TimeIntervalModel[]>(environment.api_url + 'time_intervals.json').pipe(share());
  }
}
