import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {CommutingRelationsComponent} from './components/commuting-relations/commuting-relations.component';
import {CommutingComponent} from './components/commuting/commuting.component';
import {InfoComponent} from './components/info/info.component';

const routes: Routes = [
    { path: '', component: HomeComponent, data: {title: 'Úvod'} },
    { path: 'commuting-relations', component: CommutingRelationsComponent, data: {title: 'Dojížďkové vztahy'} },
    { path: 'commuting', component: CommutingComponent, data: {title: 'Vyjížďka a dojížďka'} },
    { path: 'info/:page', component: InfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
