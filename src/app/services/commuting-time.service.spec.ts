import { TestBed, inject } from '@angular/core/testing';

import { CommutingTimeService } from './commuting-time.service';

describe('CommutingTimeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingTimeService]
    });
  });

  it('should be created', inject([CommutingTimeService], (service: CommutingTimeService) => {
    expect(service).toBeTruthy();
  }));
});
