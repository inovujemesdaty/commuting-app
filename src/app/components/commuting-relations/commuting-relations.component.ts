import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {Observable} from 'rxjs';
import {Snapshot} from '../../models/snapshot.model';
import {SnapshotService} from '../../services/snapshot.service';
import {Location} from '../../models/location.model';
import {LocationService} from '../../services/location.service';
import {ActivatedRoute} from '@angular/router';
import {CommutingRelationsModel} from '../../models/commuting-relations.model';
import {UrlgenService} from '../../services/urlgen.service';
import {AppSettings} from '../../app.settings';
import {Chart} from 'chart.js';
import {Select, Store} from '@ngxs/store';
import {CommutingRelationsService} from '../../services/commuting-relations.service';

@Component({
  selector: 'app-commuting-relations',
  templateUrl: './commuting-relations.component.html'
})
export class CommutingRelationsComponent implements OnInit, AfterViewInit {

  public rows$: Observable<CommutingRelationsModel[]>;
  public columns = [
//    { prop: 'id', 'name' : 'ID' },
    { prop: 'sourceLocation.name', 'name' : 'Zdrojová oblast' },
    { prop: 'sourceLocation.type', 'name' : 'Typ zdr. obl.' },
    { prop: 'destinationLocation.name', 'name' : 'Cílová oblast' },
    { prop: 'destinationLocation.type', 'name' : 'Typ cíl. obl.' },
    { prop: 'snapshot.name', 'name' : 'Snapshot' },
    { prop: 'count', 'name' : 'Počet pracujících' },
  ];
  public indicator = false;
  public firstLoad = true;
  public snapshots$: Observable<Snapshot[]>;
  @Select(state => state.location.locations) locations$: Observable<Location[]>;
  public locations = [];
  public selectedSnapshots$ = [];
  public selectedLocations$: Observable<Location[]>;
  public selectedDestinations$: Observable<Location[]>;
  public messages = AppSettings.ngxTableMessages;
  public topGraphData;
  public topGraphDataBySnapshot = [];
  public topGraphType = 'doughnut';
  public topGraphOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    title: {
      display: false,
      text: ''
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };
  public layers = [];
  public layersLocations = [];
  public map: L.Map;
  public csvUrl = '';
  // options
  showLegend = true;

  colorScheme = {
    domain: AppSettings.graphColors.domain
  };

  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  public options = {
    layers: [
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 14, minZoom: 6})
    ],
    zoom: 7,
    center: L.latLng( 49.774169, 14.704379)
  };

  chart = [];

  constructor(
      private snapshotService: SnapshotService,
      private locationService: LocationService,
      private commutingService: CommutingRelationsService,
      private urlGen: UrlgenService,
      private activeRoute: ActivatedRoute,
      private store: Store) {}

  ngOnInit() {
    this.snapshots$ = this.snapshotService.getSnapshots();
    // this.locations$ = this.locationService.getLocations();
    this.locations$.subscribe(data => {
      this.locations = data;
    });
  }

  updateData() {
    this.indicator = true;
    this.rows$ = this.commutingService.getCommutingBetweenLocations(
      this.selectedSnapshots$,
      this.selectedLocations$,
      this.selectedDestinations$
    );
    if (this.rows$ !== null) {
      this.rows$.subscribe((x) => {
          this.indicator = false;
          this.topGraphData = this.commutingService.getTopCommutingBetweenGraphjsData(x);
          this.topGraphDataBySnapshot = this.commutingService.getCommutingBetweenGraphjsData(x);
          this.layers = this.commutingService.getReturnedLayers(this.locations, x, this.layersLocations, this.selectedLocations$);
        },
        () => {
          this.indicator = false;
        },
        () => {
          this.indicator = false;
        });
      this.csvUrl = this.urlGen.setUrlAction('commuting_between_locations')
        .setExtension('csv')
        .addParam('snapshot', this.selectedSnapshots$)
        .addParam('sourceLocation', this.selectedLocations$)
        .addParam('destinationLocation', this.selectedDestinations$)
        .getUrl();
    } else {
      this.indicator = false;
      this.csvUrl = '';
      this.layers = [];
      this.topGraphData = {};
    }
  }

  onLocationChange($event) {
    this.layers = $event.map(city => {
      return L.geoJSON(JSON.parse(city.border),
        { style: () => ({ color: '#D62F2F', fillOpacity: 0.65 })});
    });
    this.layersLocations = this.layers;
    this.updateData();
  }

  onDestinationChange($event) {
    this.layers = [];
    this.layersLocations = [];
    this.updateData();
  }

  onSnapshotChange($event) {
    this.updateData();
  }

  onMapReady(map: L.Map) {
    this.map = map;
  }

  ngAfterViewInit() {
    this.map.invalidateSize();
    this.map.panTo([49.774169, 15.704379]);
  }

  scrollToEl(el: HTMLElement) {
    el.scrollIntoView({behavior: 'smooth', block: 'start'});
  }
}
