import {Component, OnInit, ViewChild, TemplateRef, OnDestroy} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings} from '../../app.settings';
import {ActivatedRoute} from '@angular/router';
import { Title} from '@angular/platform-browser';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html'
})
export class InfoComponent implements OnInit, OnDestroy {

  private sub: any;
  public viewContent: any;
  public banner: number;

  constructor(private http: HttpClient, private route: ActivatedRoute, private titleService: Title){}

  ngOnInit() {
    this.banner = Math.floor(Math.random() * 2.999) + 3;
    this.sub = this.route.params.subscribe(params => {
      let page = params.page;
      AppSettings.infoPages.map((infoPage) => {
        if (page === infoPage.page) {
          this.titleService.setTitle(infoPage.title + ' | ' + AppSettings.appTitle);
          this.http.get(infoPage.url, {responseType: 'text'}).subscribe(data => {
              this.viewContent = data;
              this.banner = Math.floor(Math.random() * 2.999) + 3;
            },
            error => {
              this.viewContent = '<h4>Chyba při načítání dat</h4><p>Omlouváme se, došlo k chybě při načítání informací.</p>';
            }
          );
        }
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
