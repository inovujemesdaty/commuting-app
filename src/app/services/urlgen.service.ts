import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UrlgenService {

  private host: string;

  private urlAction: string;

  private extension: string;

  private i: number;

  private params;

  constructor() {
    this.host = environment.api_url;
    this.urlAction = '';
    this.extension = '';
    this.params = [];
  }

  setHost(host: string) {
    this.host = host;
    return this;
  }

  setUrlAction(url: string) {
    this.urlAction = url;
    return this;
  }

  setExtension(extension: string) {
    this.extension = extension;
    return this;
  }

  addParam(name: string, value: any) {
    if (typeof value === 'undefined') {
      return this;
    }
    this.params[name] = value;
    return this;
  }

  getExtension() {
    if (this.extension !== '') {
      return '.' + this.extension;
    }

    return '';
  }

  getUrl() {
    return this.host + this.urlAction + this.getExtension() + this.generateParamsString();
  }

  getAmp() {
    this.i++;
    if (this.i > 1) {
      return '&';
    }
    return'';
  }

  isParameterSet(name: string) {
    if (this.params.hasOwnProperty(name)) {
      if (typeof this.params[name] === 'string'
        || (typeof this.params[name] === 'object' && this.params[name].length > 0)) {
        return true;
      }
    }
    return false;
  }

  generateParamsString() {
    let paramsString = '';
    this.i = 0;
    for (let key in this.params) {
      if (this.params.hasOwnProperty(key)) {
        if (typeof this.params[key] === 'string' || (typeof this.params[key] === 'object' && this.params[key].length === 1)) {
          paramsString = paramsString + this.getAmp() +key + '=' + this.params[key];
        }
        if (typeof this.params[key] === 'object' && this.params[key].length > 1) {
          for (let k in this.params[key]) {
            paramsString = paramsString + this.getAmp() + key + '[]=' + this.params[key][k];
          }
        }
      }
    }


    if (paramsString !== '') {
      paramsString = '?' + paramsString;
    }

    return paramsString;
  }
}
