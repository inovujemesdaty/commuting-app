import { TestBed, inject } from '@angular/core/testing';

import { CommutingRegularityService } from './commuting-regularity.service';

describe('CommutingRegularityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingRegularityService]
    });
  });

  it('should be created', inject([CommutingRegularityService], (service: CommutingRegularityService) => {
    expect(service).toBeTruthy();
  }));
});
