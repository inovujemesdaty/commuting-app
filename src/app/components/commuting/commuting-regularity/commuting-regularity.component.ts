import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Snapshot} from '../../../models/snapshot.model';
import {Location} from '../../../models/location.model';
import {AppSettings} from '../../../app.settings';
import {UrlgenService} from '../../../services/urlgen.service';
import {CommutingRegularityModel} from '../../../models/commuting-regularity.model';
import {CommutingRelationsService} from '../../../services/commuting-relations.service';
import {CommutingRegularityService} from '../../../services/commuting-regularity.service';
import {CommuteDirectionModel} from '../../../models/commute-direction.model';
import {RegularityModel} from '../../../models/regularity.model';
import {RegularityService} from '../../../services/regularity.service';
import {CommuteDirectionService} from '../../../services/commute-direction.service';

@Component({
  selector: 'app-commuting-regularity',
  templateUrl: './commuting-regularity.component.html'
})
export class CommutingRegularityComponent implements OnInit, OnChanges {

  @Input() public selectedSnapshots$: Observable<Snapshot[]>;
  @Input() public selectedLocations$: Observable<Location[]>;

  public commuteDirections;
  public topGraphData;
  public messages = AppSettings.ngxTableMessages;

  public topGraphType = 'doughnut';
  public topGraphOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    title: {
      display: false,
      text: ''
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  public rows$: Observable<CommutingRegularityModel[]>;
  public columns = [
    { prop: 'location.name', 'name' : 'Zkoumaná oblast' },
    { prop: 'location.type', 'name' : 'Typ' },
    { prop: 'regularity.name', 'name' : 'Pravidelnost' },
    { prop: 'commuteDirection.name', 'name' : 'Typ migrace' },
    { prop: 'snapshot.name', 'name' : 'Snapshot' },
    { prop: 'count', 'name' : 'Počet pracujících' },
  ];
  public indicator = false;
  public locations = [];

  public csvUrl = '';

  colorScheme = {
    domain: AppSettings.graphColors.domain
  };

  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  constructor(
    private commutingService: CommutingRegularityService,
    private commuteDirectionService: CommuteDirectionService,
    private regularityService: RegularityService,
    private urlGen: UrlgenService) {}

  ngOnInit() {
    // this.updateData();
    this.commuteDirectionService.getCommuteDirections().subscribe(data => {
      this.commuteDirections = data;});
  }
  ngOnChanges() {
    this.updateData();
  }

  updateData() {
    this.indicator = true;
    this.rows$ = this.commutingService.getCommutingRegularity(
      this.selectedSnapshots$,
      this.selectedLocations$
    );
    if (this.rows$ !== null) {
      this.rows$.subscribe((x) => {
          this.indicator = false;
          this.topGraphData = this.commutingService.getTopCommutingRegularityGraphjsData(x);
        },
        () => {
          this.indicator = false;
        },
        () => {
          this.indicator = false;
        });
      this.csvUrl = this.urlGen.setUrlAction('commuting_regularities')
        .setExtension('csv')
        .addParam('snapshot', this.selectedSnapshots$)
        .addParam('sourceLocation', this.selectedLocations$)
        .getUrl();
    } else {
      this.indicator = false;
      this.csvUrl = '';
      this.topGraphData = {};
    }
  }

}
