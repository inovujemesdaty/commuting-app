import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingDayComponent } from './commuting-day.component';

describe('CommutingDayComponent', () => {
  let component: CommutingDayComponent;
  let fixture: ComponentFixture<CommutingDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
