import { TestBed, inject } from '@angular/core/testing';

import { CommutingDurationService } from './commuting-duration.service';

describe('CommutingDurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingDurationService]
    });
  });

  it('should be created', inject([CommutingDurationService], (service: CommutingDurationService) => {
    expect(service).toBeTruthy();
  }));
});
