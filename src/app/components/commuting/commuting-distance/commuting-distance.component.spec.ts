import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingDistanceComponent } from './commuting-distance.component';

describe('CommutingDistanceComponent', () => {
  let component: CommutingDistanceComponent;
  let fixture: ComponentFixture<CommutingDistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingDistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingDistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
