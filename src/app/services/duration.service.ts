import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DayModel} from '../models/day.model';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {DurationModel} from '../models/duration.model';

@Injectable({
  providedIn: 'root'
})
export class DurationService {

  constructor(private http: HttpClient) { }

  getDurations(): Observable <DurationModel[]> {
    return this.http.get<DurationModel[]>(environment.api_url + 'durations.json').pipe(share());
  }
}
