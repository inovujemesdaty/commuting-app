import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Location} from '../models/location.model';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {AgeModel} from '../models/age.model';

@Injectable({
  providedIn: 'root'
})
export class AgeService {

  constructor(private http: HttpClient) { }

  getAges(): Observable <AgeModel[]> {
    return this.http.get<AgeModel[]>(environment.api_url + 'age_groups.json').pipe(share());
  }
}
