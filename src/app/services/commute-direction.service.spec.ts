import { TestBed, inject } from '@angular/core/testing';

import { CommuteDirectionService } from './commute-direction.service';

describe('CommuteDirectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommuteDirectionService]
    });
  });

  it('should be created', inject([CommuteDirectionService], (service: CommuteDirectionService) => {
    expect(service).toBeTruthy();
  }));
});
