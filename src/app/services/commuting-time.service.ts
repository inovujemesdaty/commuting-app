import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlgenService} from './urlgen.service';
import {Observable} from 'rxjs';
import {CommutingTimeModel} from '../models/commuting-time.model';
import {share} from 'rxjs/operators';
import {AppSettings} from '../app.settings';

@Injectable({
  providedIn: 'root'
})
export class CommutingTimeService {

  constructor(private http: HttpClient, private urlGen: UrlgenService ) { }

  getCommutingTime(snapshots, locations): Observable <CommutingTimeModel[]> {
    this.urlGen.setExtension('json');
    this.urlGen.setUrlAction('commuting_times');
    this.urlGen.addParam('snapshot', snapshots);
    this.urlGen.addParam('location', locations);
    if (this.urlGen.isParameterSet('snapshot') && this.urlGen.isParameterSet('location')) {
      return this
        .http
        .get<CommutingTimeModel[]>(this.urlGen.getUrl())
        .pipe(share());
    } else {
      return null;
    }
  }

  getTopCommutingTimeGraphjsData(input) {
    if (input.length === 0) {
      return;
    }
    input = input.sort(function (a, b) {
      return b.count - a.count;
    });
    let graphData = {
      datasets: [{
        data: [],
        backgroundColor: [],
        label: 'TOP migrace dle pravidelnosti'
      }],
      labels: []
    };
    input.forEach((data, index) => {
      if (index < AppSettings.topMigrationCount) {
        graphData.labels.push(data.location.name + ' - ' + data.commuteDirection.name +
          ' ' + data.meaningOfTime.name + ' ' + data.timeInterval.name
          + ' - ' + data.count + ' (' + data.snapshot.name + ')'
        );
        graphData.datasets[0].data.push(data.count);
        graphData.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[index]);
      }
    });
    return graphData;
  }

  getCommutingTimeGraphjsData(input) {
    if (input.length === 0) {
      return;
    };
    input = input.sort(function (a, b) {
      return b.count - a.count;
    });
    let graphData = [];
    input.forEach((data) => {
      if (graphData[data.location.id] === undefined) {
        graphData[data.location.id] = {};
        if (graphData[data.location.id][data.snapshot.id] === undefined) {
          graphData[data.location.id][data.snapshot.id] = {
            id: data.snapshot.id,
            name: data.snapshot.name,
            locationId: data.location.id,
            locationName: data.location.name,
            data: {
              datasets: [],
              labels: []
            }
          };
        }
      }
      if (graphData[data.location.id][data.snapshot.id].data.datasets) {

      }
    });
    console.log(graphData);
    input.forEach((data) => {
      var length = graphData[data.location.id][data.snapshot.id].data.datasets[0].data.length;
      if (length < 10 && ((length < 2 ) ||
        data.count / graphData[data.snapshot.id].data.datasets[0].data[1]  > 0.02)){
        graphData[data.snapshot.id].data.labels.push(
          data.sourceLocation.name + ' -> ' + data.destinationLocation.name + ' - ' + data.count
        );
        graphData[data.snapshot.id].data.datasets[0].data.push(data.count);
        graphData[data.snapshot.id].data.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[length]);
      }
    });
    let graphDataFinal = [];
    graphData.forEach((data) => {
      if(data !== undefined) {
        graphDataFinal.push(data);
      }
    });
    graphDataFinal.sort(function(a, b) {
      return a.id - b.id;
    })
    return graphDataFinal;
  }
}
