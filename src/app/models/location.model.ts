export class Location {
  id: number;
  name: string;
  type: string;
  border: string;
}
