import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';
import {Location} from '../models/location.model';
import {share} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  getLocations(): Observable <Location[]> {
    return this.http.get<Location[]>(environment.api_url + 'locations.json').pipe(share());
  }
}

