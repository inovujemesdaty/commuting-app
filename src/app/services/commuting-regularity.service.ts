import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlgenService} from './urlgen.service';
import {Observable} from 'rxjs';
import {CommutingRegularityModel} from '../models/commuting-regularity.model';
import {share} from 'rxjs/operators';
import {AppSettings} from '../app.settings';

@Injectable({
  providedIn: 'root'
})
export class CommutingRegularityService {

  constructor(private http: HttpClient, private urlGen: UrlgenService ) { }

  getCommutingRegularity(snapshots, locations): Observable <CommutingRegularityModel[]> {
    this.urlGen.setExtension('json');
    this.urlGen.setUrlAction('commuting_regularities');
    this.urlGen.addParam('snapshot', snapshots);
    this.urlGen.addParam('location', locations);
    if (this.urlGen.isParameterSet('snapshot') && this.urlGen.isParameterSet('location')) {
      return this
        .http
        .get<CommutingRegularityModel[]>(this.urlGen.getUrl())
        .pipe(share());
    } else {
      return null;
    }
  }

  getTopCommutingRegularityGraphjsData(input) {
    if (input.length === 0) {
      return;
    };
    input = input.sort(function (a, b) {
      return b.count - a.count;
    });
    let graphData = {
      datasets: [{
        data: [],
        backgroundColor: [],
        label: 'TOP migrace dle pravidelnosti'
      }],
      labels: []
    };
    input.forEach((data, index) => {
      if (index < AppSettings.topMigrationCount) {
        graphData.labels.push(data.location.name + ' - ' + data.commuteDirection.name +
          ' ' + data.regularity.name
          + ' - ' + data.count + ' (' + data.snapshot.name + ')'
        );
        graphData.datasets[0].data.push(data.count);
        graphData.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[index]);
      }
    });
    return graphData;
  }

  getCommutingRegularityGraphjsData(input) {
    if (input.length === 0) {
      return;
    };
    let graphData = [];
    input.forEach((data) => {
      if (graphData[data.snapshot.id] === undefined) {
        graphData[data.snapshot.id] = {
          id: data.snapshot.id,
          name: data.snapshot.name,
          data: {
            datasets: [{
              data: [],
              backgroundColor: [],
              label: 'Migrace ' + data.snapshot.name
            }],
            labels: []
          }
        };
      }
    });
    input.forEach((data) => {
      var length = graphData[data.snapshot.id].data.datasets[0].data.length;
      if (length < 10 && ((length < 2 ) ||
        data.count / graphData[data.snapshot.id].data.datasets[0].data[1]  > 0.02)){
        graphData[data.snapshot.id].data.labels.push(
          data.sourceLocation.name + ' -> ' + data.destinationLocation.name + ' - ' + data.count
        );
        graphData[data.snapshot.id].data.datasets[0].data.push(data.count);
        graphData[data.snapshot.id].data.datasets[0].backgroundColor.push(AppSettings.graphColors.domain[length]);
      }
    });
    let graphDataFinal = [];
    graphData.forEach((data) => {
      if(data !== undefined) {
        graphDataFinal.push(data);
      }
    });
    graphDataFinal.sort(function(a, b) {
      return a.id - b.id;
    })
    return graphDataFinal;
  }
}
