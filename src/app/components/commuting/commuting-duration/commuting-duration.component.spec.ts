import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommutingDurationComponent } from './commuting-duration.component';

describe('CommutingDurationComponent', () => {
  let component: CommutingDurationComponent;
  let fixture: ComponentFixture<CommutingDurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommutingDurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommutingDurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
