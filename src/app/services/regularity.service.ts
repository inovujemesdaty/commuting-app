import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {RegularityModel} from '../models/regularity.model';

@Injectable({
  providedIn: 'root'
})
export class RegularityService implements OnInit {

  private data;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    console.log('INIT');
    this.getRegularities().subscribe(data => {
      this.data = data;
    });
  }

  getRegularities(): Observable <RegularityModel[]> {
    return this.http.get<RegularityModel[]>(environment.api_url + 'regularities.json').pipe(share());
  }
}
