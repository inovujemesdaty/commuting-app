import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Snapshot} from '../../../models/snapshot.model';
import {Location} from '../../../models/location.model';
import {AppSettings} from '../../../app.settings';
import {UrlgenService} from '../../../services/urlgen.service';
import {CommutingDistanceModel} from '../../../models/commuting-distance.model';
import {CommutingDistanceService} from '../../../services/commuting-distance.service';
import {CommuteDirectionModel} from '../../../models/commute-direction.model';
import {DistanceModel} from '../../../models/distance.model';

@Component({
  selector: 'app-commuting-distance',
  templateUrl: './commuting-distance.component.html'
})
export class CommutingDistanceComponent implements OnInit, OnChanges {

  @Input() public selectedSnapshots$: Observable<Snapshot[]>;
  @Input() public selectedLocations$: Observable<Location[]>;
  @Input() public distances$: Observable<DistanceModel[]>;
  @Input() public commuteDirections$: Observable<CommuteDirectionModel[]>;

  public topGraphData;
  public messages = AppSettings.ngxTableMessages;

  public topGraphType = 'doughnut';
  public topGraphOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    title: {
      display: false,
      text: ''
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  public rows$: Observable<CommutingDistanceModel[]>;
  public columns = [
    { prop: 'location.name', 'name' : 'Zkoumaná oblast' },
    { prop: 'location.type', 'name' : 'Typ' },
    { prop: 'distance.name', 'name' : 'Vzdálenost' },
    { prop: 'commuteDirection.name', 'name' : 'Typ migrace' },
    { prop: 'snapshot.name', 'name' : 'Snapshot' },
    { prop: 'count', 'name' : 'Počet pracujících' },
  ];
  public indicator = false;
  public locations = [];

  public csvUrl = '';

  colorScheme = {
    domain: AppSettings.graphColors.domain
  };

  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  constructor(
    private commutingService: CommutingDistanceService,
    private urlGen: UrlgenService) {}

  ngOnInit() {
    // this.updateData();
  }
  ngOnChanges() {
    this.updateData();
  }

  updateData() {
    this.indicator = true;
    this.rows$ = this.commutingService.getCommutingDistance(
      this.selectedSnapshots$,
      this.selectedLocations$
    );
    if (this.rows$ !== null) {
      this.rows$.subscribe((x) => {
          this.indicator = false;
          this.topGraphData = this.commutingService.getTopCommutingDistanceGraphjsData(x);
        },
        () => {
          this.indicator = false;
        },
        () => {
          this.indicator = false;
        });
      this.csvUrl = this.urlGen.setUrlAction('commuting_distances')
        .setExtension('csv')
        .addParam('snapshot', this.selectedSnapshots$)
        .addParam('sourceLocation', this.selectedLocations$)
        .getUrl();
    } else {
      this.indicator = false;
      this.csvUrl = '';
      this.topGraphData = {};
    }
  }

}
