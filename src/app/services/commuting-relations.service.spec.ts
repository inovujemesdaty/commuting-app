import { TestBed, inject } from '@angular/core/testing';

import { CommutingRelationsService } from './commuting-relations.service';

describe('CommutingRelationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingRelationsService]
    });
  });

  it('should be created', inject([CommutingRelationsService], (service: CommutingRelationsService) => {
    expect(service).toBeTruthy();
  }));
});
