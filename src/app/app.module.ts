import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './components/home/home.component';
import {FooterComponent} from './components/partials/footer/footer.component';
import {HeaderComponent} from './components/partials/header/header.component';
import {CommutingRelationsComponent} from './components/commuting-relations/commuting-relations.component';
import {CommutingComponent} from './components/commuting/commuting.component';
import {CommutingDayComponent} from './components/commuting/commuting-day/commuting-day.component';
import {CommutingDistanceComponent} from './components/commuting/commuting-distance/commuting-distance.component';
import {CommutingDurationComponent} from './components/commuting/commuting-duration/commuting-duration.component';
import {CommutingRegularityComponent} from './components/commuting/commuting-regularity/commuting-regularity.component';
import {CommutingTimeComponent} from './components/commuting/commuting-time/commuting-time.component';
import {CommutingAgeGenderComponent} from './components/commuting/commuting-age-gender/commuting-age-gender.component';
import {GraphjsComponent} from './components/partials/graphjs/graphjs.component';
import {HttpClientModule} from '@angular/common/http';
import {NgSelectModule} from '@ng-select/ng-select';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxsModule} from '@ngxs/store';
import { WebStorageModule } from 'ngx-store';
import {LocationState} from './state/location';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';
import {FormsModule} from '@angular/forms';
import { InfoComponent } from './components/info/info.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CommutingRelationsComponent,
    CommutingComponent,
    CommutingDayComponent,
    CommutingDistanceComponent,
    CommutingDurationComponent,
    CommutingRegularityComponent,
    CommutingTimeComponent,
    CommutingAgeGenderComponent,
    GraphjsComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    LeafletModule.forRoot(),
    NgxDatatableModule,
    BrowserAnimationsModule,
    WebStorageModule,
    NgxsModule.forRoot([
      LocationState
    ]),
    NgxsRouterPluginModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
