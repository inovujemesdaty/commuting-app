import { TestBed, inject } from '@angular/core/testing';

import { MeaningOfTimeService } from './meaning-of-time.service';

describe('MeaningOfTimeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeaningOfTimeService]
    });
  });

  it('should be created', inject([MeaningOfTimeService], (service: MeaningOfTimeService) => {
    expect(service).toBeTruthy();
  }));
});
