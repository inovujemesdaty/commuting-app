import {Snapshot} from './snapshot.model';
import {Location} from './location.model';
import {GenderModel} from './gender.model';
import {AgeModel} from './age.model';
import {CommuteDirectionModel} from './commute-direction.model';
import {DayModel} from './day.model';
import {DistanceModel} from './distance.model';
import {DurationModel} from './duration.model';

export class CommutingDurationModel {
  id: number;
  snapshot: Snapshot;
  location: Location;
  duration: DurationModel;
  commuteDirection: CommuteDirectionModel;
  count: number;
}
