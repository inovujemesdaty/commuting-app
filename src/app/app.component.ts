import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import { map, mergeMap, filter, scan } from 'rxjs/operators';
import { AppSettings } from './app.settings';

declare let gtag: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
  ) {
    if (AppSettings.googleAnalyticsCode !== '') {
      this.router.events.subscribe((event) => {
          if (event instanceof NavigationEnd) {
            gtag('config', AppSettings.googleAnalyticsCode,
              {
                page_path: event.urlAfterRedirects
              }
            );
          }
        });
    }
  }

  getContainerClass() {
    const notFluid = ['/project', '/dev'];
    if (notFluid.indexOf(this.router.url) !== -1) {
      return 'container';
    }
    return 'container-fluid';
  }

  ngOnInit() {
    this.router.events.pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {route = route.firstChild;}
          return route;
        }),
        filter((route) => route.outlet === 'primary'),
        mergeMap((route) => route.data)
      )
      .subscribe((event) => {
        if (event.title !== undefined) {
          this.titleService.setTitle(event.title + ' | ' + AppSettings.appTitle);
        }
      });
  }
}
