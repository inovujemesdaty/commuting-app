import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DayModel} from '../models/day.model';
import {environment} from '../../environments/environment';
import {share} from 'rxjs/operators';
import {GenderModel} from '../models/gender.model';

@Injectable({
  providedIn: 'root'
})
export class GenderService {

  constructor(private http: HttpClient) { }

  getGenders(): Observable <GenderModel[]> {
    return this.http.get<GenderModel[]>(environment.api_url + 'genders.json').pipe(share());
  }
}
