export class CommuteDirectionModel {
  id: number;
  name: string;
  description: string;
}
