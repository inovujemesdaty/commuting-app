import { TestBed, inject } from '@angular/core/testing';

import { CommutingAgeGenderService } from './commuting-age-gender.service';

describe('CommutingAgeGenderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommutingAgeGenderService]
    });
  });

  it('should be created', inject([CommutingAgeGenderService], (service: CommutingAgeGenderService) => {
    expect(service).toBeTruthy();
  }));
});
