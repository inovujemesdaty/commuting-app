import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphjsComponent } from './graphjs.component';

describe('GraphjsComponent', () => {
  let component: GraphjsComponent;
  let fixture: ComponentFixture<GraphjsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphjsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
